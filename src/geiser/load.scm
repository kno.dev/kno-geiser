;;; -*- Mode: Scheme; -*-

(in-module 'geiser/load)

(use-module '{logger})

(with-working-directory-pathname
    (directory-pathname (current-load-pathname))
  (lambda ()
    (load "compile.scm")
    (load-package-set "geiser"
                      `())))

;(add-subsystem-identification! "Geiser" '(0 1))
