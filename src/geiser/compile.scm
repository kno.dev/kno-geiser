;;; -*- Mode: Scheme; -*-

(in-module 'geiser/load)

(use-module '{logger})

;;(load-option 'cref)

(with-working-directory-pathname
    (directory-pathname (current-load-pathname))
  (lambda ()
    (cf "emacs")
    (cref/generate-constructors "geiser" 'all)))
